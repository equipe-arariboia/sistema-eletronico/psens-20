EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5EEBF3E6
P 6650 1800
AR Path="/5EEBF3E6" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3E6" Ref="R5"  Part="1" 
F 0 "R5" H 6580 1754 50  0000 R CNN
F 1 "4.7k" H 6580 1845 50  0000 R CNN
F 2 "" V 6580 1800 50  0001 C CNN
F 3 "~" H 6650 1800 50  0001 C CNN
	1    6650 1800
	-1   0    0    1   
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF3F2
P 6650 2200
AR Path="/5EEBF3F2" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3F2" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 6650 1950 50  0001 C CNN
F 1 "GNDD" H 6654 2045 50  0000 C CNN
F 2 "" H 6650 2200 50  0001 C CNN
F 3 "" H 6650 2200 50  0001 C CNN
	1    6650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1600 6650 1650
Wire Wire Line
	6650 1950 6650 2050
Wire Wire Line
	6650 2050 6650 2200
Connection ~ 6650 2050
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEBF3FC
P 7500 2050
AR Path="/5EEBF3FC" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3FC" Ref="J5"  Part="1" 
F 0 "J5" H 7528 2076 50  0000 L CNN
F 1 "Conn_InverterTemp" H 7528 1985 50  0000 L CNN
F 2 "" H 7500 2050 50  0001 C CNN
F 3 "~" H 7500 2050 50  0001 C CNN
	1    7500 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2050 7300 2050
Wire Wire Line
	7300 1950 7200 1950
$Comp
L power:+5V #PWR?
U 1 1 5EEBF404
P 7200 1950
AR Path="/5EEBF404" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF404" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 7200 1800 50  0001 C CNN
F 1 "+5V" H 7215 2123 50  0000 C CNN
F 2 "" H 7200 1950 50  0001 C CNN
F 3 "" H 7200 1950 50  0001 C CNN
	1    7200 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF40B
P 7200 2200
AR Path="/5EEBF40B" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF40B" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 7200 1950 50  0001 C CNN
F 1 "GNDD" H 7204 2045 50  0000 C CNN
F 2 "" H 7200 2200 50  0001 C CNN
F 3 "" H 7200 2200 50  0001 C CNN
	1    7200 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2150 7200 2150
Wire Wire Line
	7200 2150 7200 2200
$Comp
L Device:R R?
U 1 1 5EEBF413
P 3950 3000
AR Path="/5EEBF413" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEBF413" Ref="R2"  Part="1" 
F 0 "R2" H 3880 2954 50  0000 R CNN
F 1 "4.7k" H 3880 3045 50  0000 R CNN
F 2 "" V 3880 3000 50  0001 C CNN
F 3 "~" H 3950 3000 50  0001 C CNN
	1    3950 3000
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEBF419
P 3950 2800
AR Path="/5EEBF419" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF419" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 3950 2650 50  0001 C CNN
F 1 "+5V" H 3965 2973 50  0000 C CNN
F 2 "" H 3950 2800 50  0001 C CNN
F 3 "" H 3950 2800 50  0001 C CNN
	1    3950 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF41F
P 3950 3400
AR Path="/5EEBF41F" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF41F" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 3950 3150 50  0001 C CNN
F 1 "GNDD" H 3954 3245 50  0000 C CNN
F 2 "" H 3950 3400 50  0001 C CNN
F 3 "" H 3950 3400 50  0001 C CNN
	1    3950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2800 3950 2850
Wire Wire Line
	3950 3150 3950 3250
Wire Wire Line
	3950 3250 3950 3400
Connection ~ 3950 3250
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEBF429
P 4800 3250
AR Path="/5EEBF429" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEBF429" Ref="J2"  Part="1" 
F 0 "J2" H 4828 3276 50  0000 L CNN
F 1 "Conn_BatTemp" H 4828 3185 50  0000 L CNN
F 2 "" H 4800 3250 50  0001 C CNN
F 3 "~" H 4800 3250 50  0001 C CNN
	1    4800 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3250 4600 3250
Wire Wire Line
	4600 3150 4500 3150
$Comp
L power:+5V #PWR?
U 1 1 5EEBF431
P 4500 3150
AR Path="/5EEBF431" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF431" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 4500 3000 50  0001 C CNN
F 1 "+5V" H 4515 3323 50  0000 C CNN
F 2 "" H 4500 3150 50  0001 C CNN
F 3 "" H 4500 3150 50  0001 C CNN
	1    4500 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF438
P 4500 3400
AR Path="/5EEBF438" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF438" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 4500 3150 50  0001 C CNN
F 1 "GNDD" H 4504 3245 50  0000 C CNN
F 2 "" H 4500 3400 50  0001 C CNN
F 3 "" H 4500 3400 50  0001 C CNN
	1    4500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3350 4500 3350
Wire Wire Line
	4500 3350 4500 3400
$Comp
L Device:R R?
U 1 1 5EEBF440
P 6650 3000
AR Path="/5EEBF440" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEBF440" Ref="R6"  Part="1" 
F 0 "R6" H 6580 2954 50  0000 R CNN
F 1 "4.7k" H 6580 3045 50  0000 R CNN
F 2 "" V 6580 3000 50  0001 C CNN
F 3 "~" H 6650 3000 50  0001 C CNN
	1    6650 3000
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEBF446
P 6650 2800
AR Path="/5EEBF446" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF446" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 6650 2650 50  0001 C CNN
F 1 "+5V" H 6665 2973 50  0000 C CNN
F 2 "" H 6650 2800 50  0001 C CNN
F 3 "" H 6650 2800 50  0001 C CNN
	1    6650 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF44C
P 6650 3400
AR Path="/5EEBF44C" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF44C" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 6650 3150 50  0001 C CNN
F 1 "GNDD" H 6654 3245 50  0000 C CNN
F 2 "" H 6650 3400 50  0001 C CNN
F 3 "" H 6650 3400 50  0001 C CNN
	1    6650 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2800 6650 2850
Wire Wire Line
	6650 3150 6650 3250
Wire Wire Line
	6650 3250 6650 3400
Connection ~ 6650 3250
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEBF456
P 7500 3250
AR Path="/5EEBF456" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEBF456" Ref="J6"  Part="1" 
F 0 "J6" H 7528 3276 50  0000 L CNN
F 1 "Conn_Panel1Temp" H 7528 3185 50  0000 L CNN
F 2 "" H 7500 3250 50  0001 C CNN
F 3 "~" H 7500 3250 50  0001 C CNN
	1    7500 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3250 7300 3250
Wire Wire Line
	7300 3150 7200 3150
$Comp
L power:+5V #PWR?
U 1 1 5EEBF45E
P 7200 3150
AR Path="/5EEBF45E" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF45E" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 7200 3000 50  0001 C CNN
F 1 "+5V" H 7215 3323 50  0000 C CNN
F 2 "" H 7200 3150 50  0001 C CNN
F 3 "" H 7200 3150 50  0001 C CNN
	1    7200 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF465
P 7200 3400
AR Path="/5EEBF465" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF465" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 7200 3150 50  0001 C CNN
F 1 "GNDD" H 7204 3245 50  0000 C CNN
F 2 "" H 7200 3400 50  0001 C CNN
F 3 "" H 7200 3400 50  0001 C CNN
	1    7200 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3350 7200 3350
Wire Wire Line
	7200 3350 7200 3400
$Comp
L Device:R R?
U 1 1 5EEBF46D
P 3950 4150
AR Path="/5EEBF46D" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEBF46D" Ref="R3"  Part="1" 
F 0 "R3" H 3880 4104 50  0000 R CNN
F 1 "4.7k" H 3880 4195 50  0000 R CNN
F 2 "" V 3880 4150 50  0001 C CNN
F 3 "~" H 3950 4150 50  0001 C CNN
	1    3950 4150
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEBF473
P 3950 3950
AR Path="/5EEBF473" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF473" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 3950 3800 50  0001 C CNN
F 1 "+5V" H 3965 4123 50  0000 C CNN
F 2 "" H 3950 3950 50  0001 C CNN
F 3 "" H 3950 3950 50  0001 C CNN
	1    3950 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF479
P 3950 4550
AR Path="/5EEBF479" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF479" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 3950 4300 50  0001 C CNN
F 1 "GNDD" H 3954 4395 50  0000 C CNN
F 2 "" H 3950 4550 50  0001 C CNN
F 3 "" H 3950 4550 50  0001 C CNN
	1    3950 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3950 3950 4000
Wire Wire Line
	3950 4300 3950 4400
Wire Wire Line
	3950 4400 3950 4550
Connection ~ 3950 4400
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEBF483
P 4800 4400
AR Path="/5EEBF483" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEBF483" Ref="J3"  Part="1" 
F 0 "J3" H 4828 4426 50  0000 L CNN
F 1 "Conn_Panel2Temp" H 4828 4335 50  0000 L CNN
F 2 "" H 4800 4400 50  0001 C CNN
F 3 "~" H 4800 4400 50  0001 C CNN
	1    4800 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 4400 4600 4400
Wire Wire Line
	4600 4300 4500 4300
$Comp
L power:+5V #PWR?
U 1 1 5EEBF48B
P 4500 4300
AR Path="/5EEBF48B" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF48B" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 4500 4150 50  0001 C CNN
F 1 "+5V" H 4515 4473 50  0000 C CNN
F 2 "" H 4500 4300 50  0001 C CNN
F 3 "" H 4500 4300 50  0001 C CNN
	1    4500 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF492
P 4500 4550
AR Path="/5EEBF492" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF492" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 4500 4300 50  0001 C CNN
F 1 "GNDD" H 4504 4395 50  0000 C CNN
F 2 "" H 4500 4550 50  0001 C CNN
F 3 "" H 4500 4550 50  0001 C CNN
	1    4500 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 4500 4500 4500
Wire Wire Line
	4500 4500 4500 4550
$Comp
L power:+5V #PWR?
U 1 1 5EEBF3EC
P 6650 1600
AR Path="/5EEBF3EC" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3EC" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 6650 1450 50  0001 C CNN
F 1 "+5V" H 6665 1773 50  0000 C CNN
F 2 "" H 6650 1600 50  0001 C CNN
F 3 "" H 6650 1600 50  0001 C CNN
	1    6650 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2100 4500 2150
Wire Wire Line
	4600 2100 4500 2100
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF3DE
P 4500 2150
AR Path="/5EEBF3DE" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3DE" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 4500 1900 50  0001 C CNN
F 1 "GNDD" H 4504 1995 50  0000 C CNN
F 2 "" H 4500 2150 50  0001 C CNN
F 3 "" H 4500 2150 50  0001 C CNN
	1    4500 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEBF3D7
P 4500 1900
AR Path="/5EEBF3D7" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3D7" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 4500 1750 50  0001 C CNN
F 1 "+5V" H 4515 2073 50  0000 C CNN
F 2 "" H 4500 1900 50  0001 C CNN
F 3 "" H 4500 1900 50  0001 C CNN
	1    4500 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 1900 4500 1900
Wire Wire Line
	3950 2000 4600 2000
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEBF3CF
P 4800 2000
AR Path="/5EEBF3CF" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3CF" Ref="J1"  Part="1" 
F 0 "J1" H 4828 2026 50  0000 L CNN
F 1 "Conn_MotorTemp" H 4828 1935 50  0000 L CNN
F 2 "" H 4800 2000 50  0001 C CNN
F 3 "~" H 4800 2000 50  0001 C CNN
	1    4800 2000
	1    0    0    -1  
$EndComp
Connection ~ 3950 2000
Wire Wire Line
	3950 2000 3950 2150
Wire Wire Line
	3950 1900 3950 2000
Wire Wire Line
	3950 1550 3950 1600
$Comp
L power:GNDD #PWR?
U 1 1 5EEBF3C5
P 3950 2150
AR Path="/5EEBF3C5" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3C5" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 3950 1900 50  0001 C CNN
F 1 "GNDD" H 3954 1995 50  0000 C CNN
F 2 "" H 3950 2150 50  0001 C CNN
F 3 "" H 3950 2150 50  0001 C CNN
	1    3950 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEBF3BF
P 3950 1550
AR Path="/5EEBF3BF" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3BF" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 3950 1400 50  0001 C CNN
F 1 "+5V" H 3965 1723 50  0000 C CNN
F 2 "" H 3950 1550 50  0001 C CNN
F 3 "" H 3950 1550 50  0001 C CNN
	1    3950 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EEBF3B9
P 3950 1750
AR Path="/5EEBF3B9" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEBF3B9" Ref="R1"  Part="1" 
F 0 "R1" H 3880 1704 50  0000 R CNN
F 1 "4.7k" H 3880 1795 50  0000 R CNN
F 2 "" V 3880 1750 50  0001 C CNN
F 3 "~" H 3950 1750 50  0001 C CNN
	1    3950 1750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5EEE18A0
P 6650 4100
AR Path="/5EEE18A0" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEE18A0" Ref="R7"  Part="1" 
F 0 "R7" H 6580 4054 50  0000 R CNN
F 1 "4.7k" H 6580 4145 50  0000 R CNN
F 2 "" V 6580 4100 50  0001 C CNN
F 3 "~" H 6650 4100 50  0001 C CNN
	1    6650 4100
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEE18A6
P 6650 3900
AR Path="/5EEE18A6" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE18A6" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 6650 3750 50  0001 C CNN
F 1 "+5V" H 6665 4073 50  0000 C CNN
F 2 "" H 6650 3900 50  0001 C CNN
F 3 "" H 6650 3900 50  0001 C CNN
	1    6650 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEE18AC
P 6650 4500
AR Path="/5EEE18AC" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE18AC" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 6650 4250 50  0001 C CNN
F 1 "GNDD" H 6654 4345 50  0000 C CNN
F 2 "" H 6650 4500 50  0001 C CNN
F 3 "" H 6650 4500 50  0001 C CNN
	1    6650 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3900 6650 3950
Wire Wire Line
	6650 4250 6650 4350
Wire Wire Line
	6650 4350 6650 4500
Connection ~ 6650 4350
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEE18B6
P 7500 4350
AR Path="/5EEE18B6" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEE18B6" Ref="J7"  Part="1" 
F 0 "J7" H 7528 4376 50  0000 L CNN
F 1 "Conn_Panel3Temp" H 7528 4285 50  0000 L CNN
F 2 "" H 7500 4350 50  0001 C CNN
F 3 "~" H 7500 4350 50  0001 C CNN
	1    7500 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4350 7300 4350
Wire Wire Line
	7300 4250 7200 4250
$Comp
L power:+5V #PWR?
U 1 1 5EEE18BE
P 7200 4250
AR Path="/5EEE18BE" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE18BE" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 7200 4100 50  0001 C CNN
F 1 "+5V" H 7215 4423 50  0000 C CNN
F 2 "" H 7200 4250 50  0001 C CNN
F 3 "" H 7200 4250 50  0001 C CNN
	1    7200 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEE18C5
P 7200 4500
AR Path="/5EEE18C5" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE18C5" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 7200 4250 50  0001 C CNN
F 1 "GNDD" H 7204 4345 50  0000 C CNN
F 2 "" H 7200 4500 50  0001 C CNN
F 3 "" H 7200 4500 50  0001 C CNN
	1    7200 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4450 7200 4450
Wire Wire Line
	7200 4450 7200 4500
$Comp
L Device:R R?
U 1 1 5EEE3E54
P 3950 5300
AR Path="/5EEE3E54" Ref="R?"  Part="1" 
AR Path="/5EEAACEF/5EEE3E54" Ref="R4"  Part="1" 
F 0 "R4" H 3880 5254 50  0000 R CNN
F 1 "4.7k" H 3880 5345 50  0000 R CNN
F 2 "" V 3880 5300 50  0001 C CNN
F 3 "~" H 3950 5300 50  0001 C CNN
	1    3950 5300
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EEE3E5A
P 3950 5100
AR Path="/5EEE3E5A" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE3E5A" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3950 4950 50  0001 C CNN
F 1 "+5V" H 3965 5273 50  0000 C CNN
F 2 "" H 3950 5100 50  0001 C CNN
F 3 "" H 3950 5100 50  0001 C CNN
	1    3950 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEE3E60
P 3950 5700
AR Path="/5EEE3E60" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE3E60" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3950 5450 50  0001 C CNN
F 1 "GNDD" H 3954 5545 50  0000 C CNN
F 2 "" H 3950 5700 50  0001 C CNN
F 3 "" H 3950 5700 50  0001 C CNN
	1    3950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5100 3950 5150
Wire Wire Line
	3950 5450 3950 5550
Wire Wire Line
	3950 5550 3950 5700
Connection ~ 3950 5550
$Comp
L Connector:Conn_01x03_Female J?
U 1 1 5EEE3E6A
P 4800 5550
AR Path="/5EEE3E6A" Ref="J?"  Part="1" 
AR Path="/5EEAACEF/5EEE3E6A" Ref="J4"  Part="1" 
F 0 "J4" H 4828 5576 50  0000 L CNN
F 1 "Conn_Panel4Temp" H 4828 5485 50  0000 L CNN
F 2 "" H 4800 5550 50  0001 C CNN
F 3 "~" H 4800 5550 50  0001 C CNN
	1    4800 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5550 4600 5550
Wire Wire Line
	4600 5450 4500 5450
$Comp
L power:+5V #PWR?
U 1 1 5EEE3E72
P 4500 5450
AR Path="/5EEE3E72" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE3E72" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 4500 5300 50  0001 C CNN
F 1 "+5V" H 4515 5623 50  0000 C CNN
F 2 "" H 4500 5450 50  0001 C CNN
F 3 "" H 4500 5450 50  0001 C CNN
	1    4500 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDD #PWR?
U 1 1 5EEE3E79
P 4500 5700
AR Path="/5EEE3E79" Ref="#PWR?"  Part="1" 
AR Path="/5EEAACEF/5EEE3E79" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 4500 5450 50  0001 C CNN
F 1 "GNDD" H 4504 5545 50  0000 C CNN
F 2 "" H 4500 5700 50  0001 C CNN
F 3 "" H 4500 5700 50  0001 C CNN
	1    4500 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5650 4500 5650
Wire Wire Line
	4500 5650 4500 5700
Wire Wire Line
	6150 2050 6650 2050
Wire Wire Line
	6150 3250 6650 3250
Wire Wire Line
	6200 4350 6650 4350
Wire Wire Line
	3950 4400 3450 4400
Wire Wire Line
	3950 3250 3450 3250
Wire Wire Line
	3450 2000 3950 2000
Wire Wire Line
	3950 5550 3450 5550
Text HLabel 6150 3250 0    50   Input ~ 0
Panel1Bus
Text HLabel 6150 2050 0    50   Input ~ 0
InverterBus
Text HLabel 3450 2000 0    50   Input ~ 0
MotorBus
Text HLabel 3450 3250 0    50   Input ~ 0
BatBus
Text HLabel 3450 4400 0    50   Input ~ 0
Panel2Bus
Text HLabel 3450 5550 0    50   Input ~ 0
Panel4Bus
Text HLabel 6200 4350 0    50   Input ~ 0
Panel3Bus
Text Notes 4300 1100 0    50   ~ 0
One pull-up resistor for each bus of sensors used,\nwhich means one pull up resistor for each equipment sensored.\n\nEach bus requires one digital pin on the microcontroller\n
$EndSCHEMATC
