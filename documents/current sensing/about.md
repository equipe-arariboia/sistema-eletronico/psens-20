# Sobre sensoriamento de corrente por efeito hall


### Sensores de Corrente - MSA Control Industria

Para utilizar este tipo de sensor é necessário um circuito que gere um potencia de =9 e -9.
Estes potenciais são utilizados pelos amplificadores operacionais internos do sensor.

Para desenvolver esse circuito, estudar:

https://www.eidusa.com/Interface_Boards_PN15_PS.htm
https://www.maximintegrated.com/en/design/technical-documents/app-notes/5/5978.html#:~:text=A%20common%20way%20to%20generate,to%20generate%20the%20negative%20output.]

https://electronics.stackexchange.com/questions/213488/generating-plus-and-minus-voltages-for-op-amp
https://electronics.stackexchange.com/questions/331291/using-tle2426-as-opamp-rails-supplies

https://www.allaboutcircuits.com/projects/build-your-own-negative-voltage-generator/?fbclid=IwAR0cUd5EDcCLeQ2FxMvIdf809-kCM5u_x-L1gHf03Wo3y0uH1QefemD1Z0E/


http://www.electronicecircuits.com/electronic-circuits/555-negative-voltage-power-supply-circuit?fbclid=IwAR2CDQsY3TyIzHklGWauUzt3TnXNAQM4PHnsnUWCOLeeyUUsLj2LXFVi0vs