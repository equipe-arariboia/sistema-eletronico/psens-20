# psens-20

Placa de sensoriamento 2020.
Placa composta pelo sensoriamento de tensão, corrente e temperatura do sistema.
 
## Bibliotecas necessárias:
- [Arduino-LiquidCrystal-I2C-library (fdebrabander)](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library)
- [OneWire (PaulStoffregen)](https://github.com/PaulStoffregen/OneWire)
- [Arduino-Temperature-Control-Library (milesburton)](https://github.com/milesburton/Arduino-Temperature-Control-Library)
- [Arduino-DS1302 (msparks)](https://github.com/msparks/arduino-ds1302)


## Sensoriamento de temperatura


Os sensores ds18b20 funcionam com 3 conexões: GND, Vcc, Data. 
Os sensores funcionam com protocolo de comunicação OneWire,
portanto, podem se comunicar via 1 fio comum de sinal para todos os sensores utilizados.

O microcontrolador utiliza uma porta digital para criar a comunicação com todos os sensores
conectados a ela via protocolo OneWire.


Objeto DeviceAdress para 64-bit 'ROM' address de cada sensor
Ponteiros de endereços de sensores. Um ponteiro para cada porta OneWire
Cada ponteiro guardará N objetos DeviceAddress correspondentes aos N sensores em uma porta
Serão alocados com a descoberta da quantidade de sensores


Objeto da classe DallasTemperature
Métodos da classe DataTemperature -> Manipulação dos sensores nas portas OneWire
Objeto <DallasTemperature> recebe endereço de objeto <OneWire>


Objetos da class OneWire
Porta digital OneWire para cada equipamento a ser sensoriado
N sensores em cada porta
Métodos da classe OneWire -> Controle das Portas OneWire