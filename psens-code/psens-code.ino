/*
  * EQUIPE ARARIBOIA - 2020
  * Placa de sensoriamento 2020 - (psens-20);
  * Aquisição de dados de temperatura, tensão e corrente;
  * Transmissão de dados via tx e rx (barramento can);
*/

#ifndef TEMPERATURE_H
#include "src/temperature.h"
#endif

using namespace T;

float resultado = 0;
SensoredDevice batteryTemperature(10);

void setup(void)
{
  Serial.begin(9600);
  batteryTemperature.setupByIndex();
}

void loop() 
{
  // método para aquisitar dados de temperatura
  batteryTemperature.acquireTemp();
  resultado = batteryTemperature.getAvgTemp();
  Serial.println("Dados:");
  for(uint8_t i=0;i<batteryTemperature.NumSens;i++){ // Completa SensorsTemp com as temperaturas do barramento
    Serial.println(batteryTemperature.Temperatures[i]);
  }
  Serial.println("Media:");
  Serial.println(resultado);
  // método para aquisitar dados de Tensão
  
  // método para aquisitar dados de Corrente

  // método para transmitir dados
}
