#ifndef TEMPERATURE_H
#define TEMPERATURE_H
#endif

#include <OneWire.h>             // Métodos de comunicação Onewire 
#include <DallasTemperature.h>   // Métodos de comunicação e tratamento de dados dos ds18b20

// Precisão de Bits dos dados de temperatura (9, 10, 11 ou 12 bits)
#define PRECISION 10

// Indicadores de metodo utilizado
#define INDEX_METHOD 2
#define ADDRESS_METHOD 1

namespace T {
    class SensoredDevice {
        public:
            SensoredDevice();
            SensoredDevice(uint8_t sensorPin);
            float* getTemp();
            float getAvgTemp();
            float* Temperatures;
            float avg;
            unsigned short NumSens;		// Numero de sensores
            void setupByAddress();
            void setupByIndex();
            uint8_t acquireTemp();

        private:
	      	uint8_t DeviceID;		   // Identificacao definida para cada equipamento
        	uint8_t OneWirePin;	       // Porta do microcontrolador
            uint8_t TypeSetup;         // 1 = Address; 2 = Index
        	OneWire OWpin;
            DallasTemperature Bus;
            DeviceAddress *Sensors;
            void tempAlert();
    };
}