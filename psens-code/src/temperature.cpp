#include "temperature.h"
using namespace T;


SensoredDevice::SensoredDevice() {  //default
    this -> OneWirePin = 10;
    this-> Temperatures = NULL;
    this-> Sensors = NULL;
    this -> avg = 0;
    this-> TypeSetup = INDEX_METHOD; // Default by Index
}


SensoredDevice::SensoredDevice(uint8_t sensorPin) {
    this -> OneWirePin = sensorPin;
    this -> Temperatures = NULL;
    this -> Sensors = NULL;
    this -> avg = 0;
    this-> TypeSetup = INDEX_METHOD; // Default by Index
}


void SensoredDevice::setupByAddress() {
    OWpin = OneWire(OneWirePin);
    Bus = DallasTemperature(&OWpin);
    //Iniciacao de sensores
    Bus.begin();
    /*
    * Função Localiza Sensores e informa o número de sensores encontrados
    * Aloca Dinâmicamente o vetor de endereços de sensores da porta correspondente
    * Atribui a cada objeto DeviceAdress seu endereço correspondente
    */
    NumSens = Bus.getDeviceCount();
    Temperatures = new float[NumSens];
    Sensors = new DeviceAddress[NumSens];
    /*
    *  Função descobre endereço de cada sensor em uma porta OneWire
    *  Atribui endereço a cada objeto DeviceAddress de um ponteiro
    */
    for (uint8_t i=0; i<NumSens; i++){
        Bus.getAddress(Sensors[i], i); // guarda endereco do sensor indexado por i
    }
  /*  
   * Set precisao de bits em todos sensores
   * A precisão de Bits irá determinar a quantidade de casas decimais do sinal entregue pelo sensor
   * Influencia também no tempo de resposta do sensor
   */
    for (uint8_t i=0;i<NumSens;i++){
         (this->Bus).setResolution(Sensors[i], PRECISION); 
    }
    this -> TypeSetup = ADDRESS_METHOD; // By Address
    return;
}


void SensoredDevice::setupByIndex() {
    OWpin = OneWire(OneWirePin);
    Bus = DallasTemperature(&OWpin);
    //Iniciacao de sensores
    Bus.begin();
    NumSens = Bus.getDeviceCount();
    Bus.setResolution(PRECISION);
    this -> TypeSetup = INDEX_METHOD; // By Index
}


uint8_t SensoredDevice::acquireTemp(){
    if(TypeSetup == ADDRESS_METHOD){
    	Bus.requestTemperatures();
        float aux(0); // Calculo de soma e media
        uint8_t flag = 0, error = 0;
        for(uint8_t i=0;i<NumSens;i++){ // Completa SensorsTemp com as temperaturas do barramento
            this -> Temperatures[i] = Bus.getTempC(Sensors[i]);
            while(this -> Temperatures[i] > 100){
                // lcd -> tempAlert();
            }
            if (this -> Temperatures[i] > 0) {
                aux += this -> Temperatures[i];
            }
            else {
                error++;
            }
        }
        // se error for igual a NumSens, NumSens-error = 0 e aux/(NumSens-error) será uma divisão por 0
        if (error != NumSens)
            avg = aux/(NumSens-error);
        return error; // indica ultimo sensor com erro
    }
    else if (TypeSetup == INDEX_METHOD){
        Bus.requestTemperatures();
        float aux(0); // Calculo de soma e media
        uint8_t flag = 0, error = 0;

        for(uint8_t i=0;i<NumSens;i++){ // Completa SensorsTemp com as temperaturas do barramento
            this -> Temperatures[i] = Bus.getTempCByIndex(i);
            while(this -> Temperatures[i] > 100){
                // lcd -> tempAlert();
            }
            if (this -> Temperatures[i] > 0) {
                aux += this -> Temperatures[i];
            }
            else {
                error++;
            }
        }
        // se error for igual a NumSens, NumSens-error = 0 e aux/(NumSens-error) será uma divisão por 0
        if (error != NumSens)
            avg = aux/(NumSens-error);
        return error;    // indica ultimo sensor com erro
    }
}

float* SensoredDevice::getTemp() {return this -> Temperatures;}

float SensoredDevice::getAvgTemp() {return this -> avg;}
