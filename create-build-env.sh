#!/bin/bash

apt-get update
cd ~

# Install arduino-cli
apt-get install curl -y
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar xjf arduino-cli.tar.bz2
rm arduino-cli.tar.bz2
mv `ls -1` /usr/bin/arduino-cli

# Install python, pip and pyserial
apt-get install python -y
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install pyserial

# Install esp32 core
printf "board_manager:\n  additional_urls:\n    - https://dl.espressif.com/dl/package_esp32_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml
arduino-cli core install esp32:esp32 --config-file .arduino-cli.yaml

# Arduino board support like Leonardo/Uno
arduino-cli core install arduino:avr
# Or if you need SAMD21/SAMD51 support:
arduino-cli core install arduino:samd

cd -

# Install 'third-party' packages: find proper location and 'git clone'
apt-get install git -y
mkdir `arduino-cli config dump | grep sketchbook | sed 's/.*\ //'`
mkdir `arduino-cli config dump | grep sketchbook | sed 's/.*\ //'`/libraries
cd `arduino-cli config dump | grep sketchbook | sed 's/.*\ //'`/libraries
git clone https://github.com/PaulStoffregen/OneWire.git
git clone https://github.com/milesburton/Arduino-Temperature-Control-Library.git
cd -